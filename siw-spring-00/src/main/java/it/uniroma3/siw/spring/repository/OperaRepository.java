package it.uniroma3.siw.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.spring.model.Artista;
import it.uniroma3.siw.spring.model.Collezione;
import it.uniroma3.siw.spring.model.Opera;

public interface OperaRepository extends CrudRepository<Opera,Long> {
	
	@Query("SELECT o FROM Opera o WHERE titolo LIKE %?1%")
	public List<Opera> findByTitoloIsLike (String titolo);
	
	public List<Opera> findByTitolo (String titolo);
	
	public List<Opera> findByAutore (Artista autore);
	
	public List<Opera> findAll();
	
	public void deleteById(Long id);
	
	public List<Opera> findByOrderByTitolo();
	
	public List<Opera> findByOrderByDataRealizzazione();
	
	@Query("UPDATE Opera SET titolo = ?1, descrizione = ?2 WHERE id = ?3")
	@Modifying
	public void saveOrUpdate(String titolo, String descrizione, Long id);
	
	public List<Opera> findByCollezione(Collezione collezione);
}
