package it.uniroma3.siw.spring.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.spring.model.Collezione;
import it.uniroma3.siw.spring.model.Curatore;
import it.uniroma3.siw.spring.repository.CollezioneRepository;

@Service
public class CollezioneService {

	@Autowired
	private CollezioneRepository collezioneRepository;
	
	@Transactional
	public Collezione inserisci(Collezione collezione) {
		return collezioneRepository.save(collezione);
	}
	
	@Transactional
	public List<Collezione> collezioniPerNome(String nome){
		return (List<Collezione>) collezioneRepository.findByNome(nome);
	}
	
	@Transactional
	public List<Collezione> collezioniPerNomeLike(String nome){
		return (List<Collezione>) collezioneRepository.findByNomeIsLike(nome);
	}
	
	@Transactional
	public List<Collezione> tutti(){
		return (List<Collezione>) collezioneRepository.findAll();
	}
	
	@Transactional
	public Collezione collezionePerId(Long id) {
		Optional<Collezione> optional = collezioneRepository.findById(id);
		if (optional.isPresent())
			return optional.get();
		else 
			return null;
	}
	
	@Transactional
	public List<Collezione> collezioniPerCuratore(Curatore curatore){
		return collezioneRepository.findByCuratore(curatore);
	}
	
	@Transactional
	public boolean alreadyExists(Collezione collezione) {
		List<Collezione> collezioni = this.collezioneRepository.findByNome(collezione.getNome());
		if(collezioni.size()>0)
			return true;
		else 
			return false;
	}
	
	@Transactional
	public void cancella(Long id) {
		this.collezioneRepository.deleteById(id);
	}
	
	@Transactional
	public void update(Collezione collezione, Long id) {
		this.collezioneRepository.saveOrUpdate(collezione.getNome(), collezione.getDescrizione(), id);
	}
	
	@Transactional
	public void salvaConImmagine(Collezione collezione, String immagine) {
		collezione.setImmagine(immagine);
		this.collezioneRepository.save(collezione);
	}
}
