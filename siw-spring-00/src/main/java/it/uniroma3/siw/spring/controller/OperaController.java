package it.uniroma3.siw.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.spring.model.Opera;
import it.uniroma3.siw.spring.service.ArtistaService;
import it.uniroma3.siw.spring.service.CloudinaryService;
import it.uniroma3.siw.spring.service.CollezioneService;
import it.uniroma3.siw.spring.service.OperaService;
import it.uniroma3.siw.spring.validator.OperaValidator;

@Controller
public class OperaController {

	@Autowired
	private OperaService operaService;

	@Autowired
	private ArtistaService artistaService;

	@Autowired
	private CollezioneService collezioneService;

	@Autowired
	private CloudinaryService cloudinaryService;

	@Autowired
	private OperaValidator operaValidator;

	private Opera operaTemp;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/opera/{id}", method = RequestMethod.GET)
	public String getOpera(@PathVariable("id") Long id, Model model) {
		Opera opera = this.operaService.operaPerId(id);

		model.addAttribute("opera", opera);
		model.addAttribute("collezioni", this.collezioneService.tutti());
		return "opera.html";
	}

	@RequestMapping(value = "/opere", method = RequestMethod.GET)
	public String getOpere(Model model) {
		model.addAttribute("opere", this.operaService.tutti());
		return "opere.html";
	}


	/**AGGIUNTA**/
	@RequestMapping(value="/admin/addOpera", method = RequestMethod.GET)
	public String addOpera(Model model) {
		logger.debug("addOpera");
		model.addAttribute("opera", new Opera());
		model.addAttribute("artisti",this.artistaService.tutti());
		return "/admin/operaForm.html";
	}

	@RequestMapping(value = "/admin/inserisciOpera", method = RequestMethod.POST)
	public String newOpera(@ModelAttribute("opera") Opera opera,
			Model model, BindingResult bindingResult,
			@RequestParam(value= "foto") MultipartFile foto) 
	{
		this.operaValidator.validate(opera, bindingResult);
		if (!bindingResult.hasErrors()) {  
			logger.debug("passo alla conferma");
			operaTemp = opera;
			operaTemp.setImmagine(cloudinaryService.salvaImmagine(foto));
			return "/admin/confermaOperaForm.html";
		}
		model.addAttribute("artisti", this.artistaService.tutti());
		return "/admin/operaForm.html";
	} 

	@RequestMapping(value = "/admin/confermaOpera", method = RequestMethod.POST)
	public String confermaArtista(Model model,
			@RequestParam(value = "action") String comando) {
		model.addAttribute("opera",operaTemp);
		if(comando.equals("confirm")) {
			logger.debug("confermo e salvo dati opera");
			this.operaService.inserisci(operaTemp);
			return "redirect:/opere";
		}
		else {
			model.addAttribute("artisti", this.artistaService.tutti());
			return "/admin/operaForm.html";
		}
	}


	/**CANCELLAZIONE**/
	@RequestMapping(value="/admin/cancellaOpera", method = RequestMethod.GET)
	public String cancellaOpera(Model model) {
		model.addAttribute("opere", this.operaService.tutti());
		return "/admin/cancellaOpere.html";
	}

	@RequestMapping(value="/admin/cancellaOpera/{id}", method = RequestMethod.GET)
	public String ConfermaCancellaOpera(@PathVariable("id") Long id,Model model) {
		logger.debug(id.toString());
		this.operaService.cancellaOpera(id);
		logger.debug("CANCELLATA OPERA CON ID: "+id.toString());
		return "redirect:/admin/cancellaOpera";
	}



	@RequestMapping(value="/cancellaOpere/ordinaAlfabetico", method = RequestMethod.GET)
	public String opereAlfabetico(Model model) {
		model.addAttribute("opere", this.operaService.tutteTitoloAlfabetico());
		return "/admin/cancellaOpere.html";
	}

	@RequestMapping(value="/cancellaOpere/ordinaData", method = RequestMethod.GET)
	public String opereDataCrescente(Model model) {
		model.addAttribute("opere", this.operaService.tutteDataCrescente());
		return "/admin/cancellaOpere.html";
	}


	/**MODIFICA**/
	@RequestMapping(value = "/admin/modificaOpera/{id}", method = RequestMethod.GET)
	public String editOpera(@PathVariable("id") Long id, Model model) {
		Opera opera = this.operaService.operaPerId(id);
		model.addAttribute("opera", opera);
		return "/admin/editOpera.html";
	}

	@RequestMapping(value = "/admin/modificaOpera/{id}", method = RequestMethod.POST)
	public String confermaModifica(@PathVariable("id") Long id, 
			@RequestParam(value="foto", required=false)MultipartFile foto,
			@Validated @ModelAttribute("opera") Opera opera, Model model) {

		if(!foto.isEmpty()) {
			String nuovaFoto = this.cloudinaryService.salvaImmagine(foto);
			opera.setImmagine(nuovaFoto);
			this.operaService.inserisci(opera);
		}
		else {
			this.operaService.update(opera, id);
		}
		return "redirect:/opera/"+id;
	}

	@RequestMapping(value="/admin/aggiungiOperaACollezione/{id}", method = RequestMethod.POST )
	public String aggiungiOperaCollezione(Model model,@PathVariable("id")Long id,
			@RequestParam(value="collezioneSelezionata") Long idColl) {
		Opera opera = this.operaService.operaPerId(id);
		opera.setCollezione(this.collezioneService.collezionePerId(idColl));
		this.operaService.inserisci(opera);
		return "redirect:/opera/"+id;
	}
}
