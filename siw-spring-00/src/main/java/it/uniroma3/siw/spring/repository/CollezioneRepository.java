package it.uniroma3.siw.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.spring.model.Collezione;
import it.uniroma3.siw.spring.model.Curatore;

public interface CollezioneRepository extends CrudRepository<Collezione,Long> {

	@Query("SELECT c FROM Collezione c WHERE nome LIKE %?1%")
	public List<Collezione> findByNomeIsLike(String nome);
	
	public List<Collezione> findByNome(String nome);
	
	public List<Collezione> findAll();
	
	public List<Collezione> findByCuratore(Curatore curatore);
	
	public void deleteById(Long id);
	
	@Query("UPDATE Collezione SET nome = ?1, descrizione = ?2  WHERE id = ?3")
	@Modifying
	public void saveOrUpdate(String nome, String descrizione, Long id);

}
