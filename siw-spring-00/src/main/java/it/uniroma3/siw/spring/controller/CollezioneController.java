package it.uniroma3.siw.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.spring.model.Collezione;
import it.uniroma3.siw.spring.service.CloudinaryService;
import it.uniroma3.siw.spring.service.CollezioneService;
import it.uniroma3.siw.spring.service.CuratoreService;
import it.uniroma3.siw.spring.service.OperaService;
import it.uniroma3.siw.spring.validator.CollezioneValidator;

@Controller
public class CollezioneController {
	
	@Autowired
	private CollezioneService collezioneService;
	
	@Autowired
	private CuratoreService curatoreService;
	
	@Autowired
	private CloudinaryService cloudinaryService;
	
	@Autowired
	private OperaService operaService;
	
	@Autowired
	private CollezioneValidator collezioneValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	/*Si occupa di gestire la richiesta quando viene selezionata
	 * una collezione dalla pagina delle varie collezioni*/
	@RequestMapping(value="/collezione/{id}",method=RequestMethod.GET)
	public String getCollezione(@PathVariable("id") Long id, Model model) {
		Collezione c = this.collezioneService.collezionePerId(id);
		model.addAttribute("collezione", c);
		model.addAttribute("opereCollezione",this.operaService.operePerCollezione(c));
		return "collezione.html";
		
	}
	
	/*Si occupa di gestire la richiesta quando viene selezionato
	 * il link della pagina collezioni*/
	@RequestMapping(value="/collezioni",method= RequestMethod.GET)
	public String getCollezioni(Model model) {
		model.addAttribute("collezioni", this.collezioneService.tutti());
		return "collezioni.html";
	}
	
	
	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/admin/addCollezione", method = RequestMethod.GET)
	public String addCollezione(Model model) {
		logger.debug("addCollezione");
		model.addAttribute("collezione", new Collezione());
		model.addAttribute("curatori",this.curatoreService.tutti());
		return "/admin/collezioneForm.html";
		
	}
	
	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/admin/inserisciCollezione", method = RequestMethod.POST)
	public String newCollezione(@ModelAttribute("collezione") Collezione collezione,
			Model model, BindingResult bindingResult,
			@RequestParam(value="foto")MultipartFile foto) {
		this.collezioneValidator.validate(collezione, bindingResult);
		if (!bindingResult.hasErrors()) {
			this.collezioneService.salvaConImmagine(collezione, this.cloudinaryService.salvaImmagine(foto));
			return "redirect:/collezioni";
		}
		return "/admin/collezioneForm.html";
	}
	
	
	/**CANCELLAZIONE**/
	@RequestMapping(value="/admin/cancellaCollezione/{id}", method = RequestMethod.GET)
	public String cancellaCollezione(@PathVariable("id") Long id,Model model) {
		logger.debug(id.toString());
		this.collezioneService.cancella(id);
		logger.debug("CANCELLATA COLLEZIONE CON ID: "+id.toString());
		return "redirect:/collezioni";
	}

	
	/**MODIFICA**/
	@RequestMapping(value="/admin/modificaCollezione/{id}", method = RequestMethod.GET)
	public String editCollezione(@PathVariable("id")Long id, Model model) {
		Collezione collezione = collezioneService.collezionePerId(id);
		logger.debug("modifica della collezione" + collezione.getNome());
		model.addAttribute("collezione",collezione);
		return "admin/editCollezione.html";
	}
	
	@RequestMapping(value="/admin/modificaCollezione/{id}", method = RequestMethod.POST)
	public String confermaModifica(@PathVariable("id")Long id, @Validated @ModelAttribute Collezione collezione, Model model) {
		return "redirect:/collezioni";
	}
}
